# Getting Started with Hugo

This project was bootstrapped with a blueprint that generated a Static Website on AWS using [Hugo](https://gohugo.io/). 

## Editing Your Project

Assuming you are reading this in the root of your CodeCatalyst Source Repository, select **Create dev environment** at the top right of the screen and select your preferred Integrated Development Environment (IDE). It may take a minute to launch and install Hugo. If you are curious how the Dev Environment is configured, you can review **devfile.yaml** in the root of this project and read more at [devfile.io](https://devfile.io/). 

## Add a New Post

Hugo is configured and you are ready to create your first post. Run the following command to create a new post. 

```
cd static-website-content
hugo new posts/my-first-post.md
```

Open static-website-content/content/posts/my-first-post.md and add some content. This file should include some meta-data about the post. Hugo calls this [Front Matter](https://gohugo.io/content-management/front-matter/). Notice that `draft: true`. If you leave this, the post will not be rendered when you commit your changes. Therefore, I changed mine to `draft: false`. Below the front matter, you can enter anything you want, using standard markdown. For example:

```
---
title: "My First Post"
date: 2019-03-26T08:47:11+01:00
draft: false
---
## Hello from CodeCatalyst!

CodeCatalyst is a cloud-based collaboration space for software development teams. CodeCatalyst provides
one place where you can plan work, collaborate on code, and build, test, and deploy applications with
continuous integration/continuous delivery (CI/CD) tools. You can easily integrate with and leverage
AWS resources with your projects by connecting your AWS accounts to your CodeCatalyst organization. With
all the stages and aspects of an application's lifecycle in one tool, you can deliver software quickly and
confidently.
```

## Commit Your Changes

Feel free to enter a few more posts. When you are ready, commit your changes and push them to CodeCatalyst. For example:

```
git add .
git commit -m 'Added my first post'
git push
```

## Observe the Workflow


Use the left navigation to find **CI/CD > Workflows** and select either **OnPushBuildAndTest** or **OnPushBuildTestAndDeploy** depending on the deployment option you chose when creating the project. The workflow started when the blueprint created this project. Select the actions in the workflow to see details of the execution. Notice that the **Build** step is using simple shell command to build the content. 



## Next Steps

Now that your site is up and running, you can pick up where we left off in the Hugo quick start by opening [configure the site](https://gohugo.io/getting-started/quick-start/#configure-the-site). If you chose the Continuous Delivery option, you will likely want to add a custom domain name to your site. You can read more in the [Amplify User Guide](https://docs.aws.amazon.com/amplify/latest/userguide/custom-domains.html).